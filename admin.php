<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<TITLE>Update Page</TITLE>
</HEAD>
<BODY>
<h3>Update Page</h3>
use with care
 <p>
<a href="http://validator.w3.org/check/referer"><img border="0"
        src="http://www.w3.org/Icons/valid-html401"
        alt="Valid HTML 4.01!" height="31" width="88"></a>
</p>
<hr>


<?PHP
# 
# 
#

$weekdaynames[0] = 'Sun.';
$weekdaynames[1] = 'Mon.';
$weekdaynames[2] = 'Tue.';
$weekdaynames[3] = 'Wed.';
$weekdaynames[4] = 'Thu.';
$weekdaynames[5] = 'Fri.';
$weekdaynames[6] = 'Sat.';


putenv("TZ=America/Chicago");   
#error_reporting(0);     

# names of files
$filen = '_avhs_sched.data';
$anfn = 'ano.data';
$resfn = 'results.data';


# test for ssl
if (!is_ssl()) {   
  echo "permission denied";
  exit;
}

if ($GLOBALS['REQUEST_METHOD'] == 'GET') {
  login();
}

else {
  echo $method."<br>";
  if ($action == "login"){
    auth($uname, $pphra);
    exit;
  }

  if ($action == "update"){
    update($sesionid, $array, $announcement, $results);
    exit;
  }

}
exit;
# should never get here!!

function is_ssl($port=443) {
# from php faqts
# if(@$GLOBALS["HTTP_SERVER_VARS"]["SERVER_PORT"]!=$port) return false;
# if(@$GLOBALS["HTTP_SERVER_VARS"]["HTTPS"]!="on") return false;
 return true;
}


function login() {
# write up the login screen and submit query

global $PHP_SELF; # The path and name of this file

$HTML =<<<HTML
<FORM ACTION="$PHP_SELF" METHOD="POST">
<input type="hidden" name="action" value="login">
<p>When you click the login button you may have to wait upto 5 seconds for the 
server to prepare the update form do not click the button twice
<p><table>
<tr><td>admin name</td><td><input type="text" name="uname"
size="30"></td></tr>
<tr><td>pass-phrase</td><td><input type="password" name="pphra"
size="30"></td></tr>
</table>
<input type="submit" value="login">
</form>
HTML;

echo $HTML;
}


function auth($username, $passwd) {
  # check login info 
  $euid = 'fe40fb944ee700392ed51bfe84dd4e3d';
  $epw = 'ac575e3eecf0fa410518c2d3a2e7209f';

  if ((md5($username) == $epw) && (md5($passwd) == $euid)) {
    #check that a lock file is not already in place (not implimented)
    echo "Authentication succeeded!<br>";    
    # generate unique session ID
    $e_this = date(r)."unFFoNstrHFssSet234432652::";
    $sid = md5($e_this);
    #store session id somewhere (file ala counter?) lockfile
    #(not implimented yet)
    printcal($sid);
    exit;
  }

  else {
    echo "Authentication failed!";
    exit;
  }
}



function printcal($id) {
# produce a dynamic form set for updateing the calender bits
#  set uid (not implimented)
  global $PHP_SELF;
  global $filen;
  global $anfn;
  global $resfn;
  global $weekdaynames;

  $file = file($filen);
  $date =  date(m) ."-". date(d)."-" .date(y);
  $fancy_date = date(l)." the ".date(jS)." of ".date(F)."<br>";
  $start = 0;

$formhead =<<<FHEAD
<FORM ACTION="$PHP_SELF" METHOD="POST">
<input type="hidden" name="action" value="update">
<input type="hidden" name="sesionid" value="$id">
<input type="submit" value="update calendar, results, and announcement" ><br>
FHEAD;

 echo $formhead;


 foreach ($file as $line) {
   # this loop gets the values for the calendar entrys from disk
  if (preg_match("/^(\d{2}-\d{2}-\d{2})\s+V\s+>>(.*)<<\s+JVB\s+>>(.*)<<\s+JVG\s+>>(.*)<<\s+9\s+>>(.*)<<\s+E\s+>>(.*)<</", $line, $match)) {

     if (($match[1] == $date) ) { $start++; }
     if ($start) {$results[] = $match;}
   }
 }

 $todaybgc = "#00ff99";
 $odaybgc = "#FFCCCC";

# update message poorly assembled vagueness
$text=<<<TEXT
<p>To make changes change the text in the boxes below for the
appropriate day. The text will automatically wrap around in the boxed
in the coming page if you
  want to force a break use the HTML tag &lt;BR&gt.
<p> Words, numbers, spaces, the  &lt;BR&gt tag, :, !, and - are allowed. This is to ensure the data file does not break . this can change if your needs change. 
<p>If errors are raised use the back button to
make corrections. a flock mechanism is used but to ensure good results only one person should us this at a time.
<hr>
<p>update the announcement box here
<p>
TEXT;

 echo $text;

 # get the anouncement from disk and put it into a <textarea>
 $old = file($anfn);

 echo "<textarea type=text name=announcement cols=50 rows=10>";
 foreach ($old as $l) {echo htmlentities($l);} 
 echo "</textarea><hr>";
 echo "<hr>\n";
 
 echo "<p> update the results here. Format is:";
 echo "<p><table border=1 bgcolor=grey width=100%><tr><td>mm-dd-yy >>> string describing result</td></tr></table>";
 
 $res = file($resfn);
 echo "<textarea type=text name=results cols=80 rows=25>";
 foreach ($res as $l) {echo htmlentities($l);} 
 echo "</textarea>";
 echo "<hr>\n";


 echo "<table border=1>";
 # table for the cal bits (adds line breaks to html file for debuging
 foreach ($results as $day) {
# day of the week
  if (preg_match("/(\d{2})-(\d{2})-(\d{2})/", $day[1], $datearray )) {
    $datehash = getdate(mktime(12,0,0,$datearray[1], $datearray[2], $datearray[3]));
    $dayofweek = $weekdaynames[$datehash['wday']];
  }
   if ($day[1] == $date) {
     echo "<tr><td bgcolor=\"$todaybgc\">".$dayofweek." <br>".stripslashes(htmlentities($day[1]))."</td><td>\n";
   }
   else {
     echo "<tr><td bgcolor=\"$odaybgc\">".$dayofweek." <br> ".stripslashes(htmlentities($day[1]))."</td><td>\n";
   }

   echo '<input type="hidden" name="array[]" value="'.stripslashes(htmlentities($day[1])).'">
';
   echo  '<table border=1 width="100%">
';  
   echo  '<tr><td>Vars</td><td><input type=text maxlength=180 size=180 name="array[]" value="'.stripslashes(htmlentities($day[2])).'"></td></tr>
';
   echo '<tr><td>JVb</td><td><input type=text  maxlength=180 size=180 name="array[]" value="'.stripslashes(htmlentities($day[3])).'"></td></tr>
';
   echo  '<tr><td>JVg</td><td><input type=text maxlength=180 size=180 name="array[]" value="'.stripslashes(htmlentities($day[4])).'"></td></tr>
';
   echo '<tr><td>9th</td><td><input type=text maxlength=180 size=180 name="array[]" value="'.stripslashes(htmlentities($day[5])).'"></td></tr>
';
   echo ' <tr><td>E</td><td><input type=text maxlength=180 size=180 name="array[]" value="'.stripslashes(htmlentities($day[6])).'"></td></tr>
';
   echo " </table><br>
";
 }

 echo " </table><br>
";
 echo "</form>
";
echo "</body></html>
";

}

function update($sesid, $calarray, $abody, $resbody){
# verify input to make sure it is not going to mung the 
# regrex. if OK and if ssl and if session id matches (not implimented)
# update
# also update anounouncment added jun 24th `02

  global $filen;
  global $anfn;
  global $resfn;

  # handel the anouncement update first
  # assume what the user enters is OK (probably bad)
  # length limit and tag checker needed here
  # also some error checking would be good here!
  $anofilep = fopen($anfn, "w-");  
  flock($anofilep, LOCK_EX);
  fputs($anofilep, stripslashes($abody));
  flock($anofilep, LOCK_UN);
  fclose($anofilep);

  // deal with the results here //
  $resultFilePointer = fopen($resfn, "w-");
  flock($resultFilePointer, LOCK_EX);
  fputs($resultFilePointer, stripslashes($resbody));
  flock($resultFilePointer, LOCK_UN);
  fclose($resultFilePointer);

  // process the cal array
  foreach ($calarray as $line) {
    # check each line for things we dont want
    # this will be changed soon
    # this can change now because the new format allows for anything but << and >>
    $c++;
    if (preg_match('/[^\w .:!-<>@]/i', $line)) {
      $errors++;
      echo "error on $line line  ($c)";
    }
  }

  if ($errors) {
    echo "Errors found strings must match /[^\w .:!-<>?@]/i";
    exit;
    echo "Should never see this message";
  }

  else {
    echo "no errors found in input... <br>";
    $schedfile = fopen($filen, "w-");
    flock($schedfile, LOCK_EX);
    for ($i=0; ($i < sizeof($calarray)); $i = $i + 6 ) {
      # break down the arary into the individiual pieces and 
      # format the data file
     # the below line needs to change also
      $line = $calarray[$i]." V >>".$calarray[$i+1]."<< JVB >>".$calarray[$i+2]."<< JVG >>".$calarray[$i+3]."<< 9 >>".$calarray[$i+4]."<< E >>".$calarray[$i+5]."<<\n";

      
      fputs($schedfile, $line);
      
    }
  flock($schedfile,LOCK_UN);
  fclose($schedfile);
  }



  echo "<h3>Update Successful !!</h3>";
  echo "<p>Inspect <a href='avhsGirlsSched.php'>the calender</a> carefully for mistakes made by the update program. If a problem is encountered contact me immediately so I can fix it. ";
  echo "<p> each entry must occupy one like only with no new line characters the HTML tag &lt;BR&gt  is ok. white space is ignored ";
} 
?>
