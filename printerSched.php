<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <TITLE>Apple Valley Tennis</TITLE>
  <LINK REL="stylesheet" HREF="vt.css" TYPE="text/css">
  <LINK REL="shortcut icon" HREF="favicon.ico">
 </HEAD>
 <BODY>


<?php
// basic mod to make a printer friendly version of this schedule thing
putenv("TZ=America/Chicago");   
error_reporting(0);             

$varsc = "#77ffe8";   # color table: varsity   #FFFF66
$jvbc  = "#ffcb8c";   # jv brown
$jvgc  = "#fff78c";   # jv gold
$ninec = "#FFCCFF";   # 9th
$anoc  = "#99CCFF";   # anoncment 


$todaybgc = "#00ff99";

$weekdaynames[0] = 'Sun.';
$weekdaynames[1] = 'Mon.';
$weekdaynames[2] = 'Tue.';
$weekdaynames[3] = 'Wed.';
$weekdaynames[4] = 'Thu.';
$weekdaynames[5] = 'Fri.';
$weekdaynames[6] = 'Sat.';


$file = file('_avhs_sched.data');
$date =  date(m) ."-". date(d)."-" .date(y);
$fancy_date = date(l)." the ".date(jS)." of ".date(F)."<br>";
$start = 0;

echo "<h1>AHVS Boys Tennis Schedule</h1>";
echo "<hr>";
echo "<h3>This scheduale is as of $fancy_date</h3>";
echo "The most up to date version is available at http://valleytennis.org";
echo "<hr>";

foreach ($file as $line) {
# $match is the array of matched things
  if (preg_match("/^(\d{2}-\d{2}-\d{2})\s+V\s+>>(.*)<<\s+JVB\s+>>(.*)<<\s+JVG\s+>>(.*)<<\s+9\s+>>(.*)<<\s+E\s+>>(.*)<</", $line, $match)) {
# with this match 
# $match[1] = the date in mm-dd-yy format
# $match[2] = Vars
#        3  = jvb
#        4  = jvg
#        5  = 9
#        6  = info for all
    if (($match[1] == $date) ) { $start++; } 
    if ($start) {$results[] = $match;}
  }
}
 
 
echo "<table border=1 width=\"100%\">";
echo "<tr><th>date</th><th>info</th></tr>";
foreach ($results as $day) {

  if (preg_match("/^(\d{2})-(\d{2})-(\d{2})/", $day[1], $datearray )) {
    $datehash = getdate(mktime(12,0,0,$datearray[1], $datearray[2], $datearray[3]));
    $dayofweek = $weekdaynames[$datehash['wday']];
      }

  if ($day[1] == $date) {echo "<tr><td>".$dayofweek." ".$day[1]."</td><td>";}
  
  else{echo "<tr><td>".$dayofweek." ".$day[1]."</td><td>";}

  if ($day[2] or $day[3] or $day[4] or $day[5] or $day[6]) {
    echo "<table border=1 width=\"100%\">";
    if ($day[2]) {
      echo "<tr><td>[ Vars ] $day[2] </td></tr>";
    }
    if ($day[3]) {
      echo "<tr><td>[ JVb ] $day[3] </td></tr>";
    }
    if ($day[4]) {
      echo "<tr><td>[ JVg ] $day[4] </td></tr>";
    }
    if ($day[5]) {
      echo "<tr><td>[ 9th ] $day[5] </td></tr>";
    }
    if ($day[6]) {
      echo "<tr><td>$day[6]</td></tr>";
    }

    echo "</table>";
  }
  

  echo "</td></tr>";
}
echo "</table>";


?>
</BODY>
</HTML>
