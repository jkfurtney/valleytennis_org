<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <TITLE>Apple Valley Tennis</TITLE>
  <LINK REL="stylesheet" HREF="vt.css" TYPE="text/css">
  <LINK REL="shortcut icon" HREF="favicon.ico">
 </HEAD>
 <BODY>
  <TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" class="bannercell">
     <A HREF="http://www.valleytennis.org/" CLASS="bannerlink">
      <IMG SRC="taller.png" ALT="" CLASS="vtbanner" WIDTH="630" HEIGHT="63">
     </A>
    </TD>
   </TR>
  </TABLE>

<!-- begin side bar -->
  <TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" >
     <TABLE BORDER="0" class="outersidebar"><TR><TD class="bordercell" VALIGN="TOP">
      <TABLE BORDER="0" CELLSPACING="3" class="vtsidebar"><TR><TD class="titlecell" VALIGN="TOP">
       <TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0" class="innersidebar">

        <TR><TD class="linkcell"><A HREF="./"><B>Apple Valley<BR>Tennis</B></A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="about.php">About the tennis<br> programs</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="coed.php">Summer Co-ed<br> Traveling Tennis<br> Team</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="tourn.php">Summer Tennis<br> Tournaments</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="camp_girls.php">Middle School<br> and High School <br>Summer Camps <br>for Girls</a> </TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt="">Apple Valley<br>High School<br> <a HREF="avhs_girls.php">Girls Team</a><br> <a href="avhs_boys.php">Boys Team</a></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><a HREF="tips.php">Previous Tips <br>of the day</a></TD></TR>

       </TABLE>
<!-- end side bar -->

       </TD>
      </TR>
     </TABLE>
     </TD>
    </TR>
   </TABLE>
   </TD>
   <TD VALIGN="TOP">




<TABLE BORDER="0"  CELLSPACING="2" CELLPADDING="0" WIDTH="100%">
 <TR>
  <TD ALIGN=CENTER VALIGN=TOP>


<!-- table for 1.0 Countdown -->
   <!-- table for black borders -->
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">

     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="titlecell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>
Previous tips of the day
          </B></TD><TD ALIGN=RIGHT NOWRAP><B>
<!-- Date of Countdown -->

          </B></TD></TR>
         </TABLE>
        </TD>
       </TR>
       <TR>
        <TD CLASS="contentcell">

         <!-- table for "content" cell with wide left/right margins -->
         <TABLE WIDTH="100%" BORDER="0" CELLPADDING="8" CELLSPACING="0">
          <TR>
           <TD WIDTH="5%"></TD>
           <TD WIDTH="90%">


<!-- cut here for template -->

<!-- Previous tips of the day -->

<ul>
<?php 
	 // reads input file and prints everything
	 // up to the current day
	 // should work for CDT
putenv("TZ=America/Chicago");   
#error_reporting(0);             
$file = file('tip_of_the_day.data');
$date =  date(m) ."-". date(d)."-" .date(y);
//$fancy_date = date(l)." the ".date(jS)." of ".date(F)."<br>";

foreach ($file as $tip) {
  if (preg_match("/^(\d{2}-\d{2}-\d{2})\s+(.*)$/", $tip, $match)) {
    if (! $gotresults) {$gotresults++;}
    $count++;
    #echo "<li>".$match[1]." -- ".$match[2]."<br>";
    $tips[$count] =  "<li>".$match[1]." -- ".$match[2]."<br>";
    if ($match[1] == $date) {
      $tip_of_the_day =  $match[2]."<br>";
      break;
    }
  }
}
# here we could invert the array and put the newest tips at the top
$tips = array_reverse($tips);

foreach ( $tips as $tip ) {
  echo $tip;
}

if (! $gotresults) {
  echo "The Previous tip code seems to be broken. Email web@valleytennis.org if you get this message -- thank you";
}

?>
</ul>


<!-- Previous tips of the day -->
<!-- cut here for template -->


           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>

     </TD>
    </TR>
   </TABLE>

  </TD>
 </TR>
</TABLE>



</TD>
</TR>
<TR>
 <TD COLSPAN="2" ALIGN="RIGHT" VALIGN="TOP">
  <div class="documentinfo">
   Web hosting by <a href="http://www.luno.net">Luno.net</a>
   <br>
    <?php 
     putenv("TZ=America/Chicago");
     echo "Last modified: " . date ("F d Y h:ia. T", getlastmod());
    ?>
   <br>email web@valleytennis.org about technical problems
  </div>
 </TD>
</TR>
</TABLE>
</BODY>
</HTML>
