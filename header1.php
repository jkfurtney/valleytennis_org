<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <TITLE>Apple Valley Tennis</TITLE>
  <LINK REL="stylesheet" HREF="vt.css" TYPE="text/css">
  <LINK REL="shortcut icon" HREF="favicon.ico">
 </HEAD>
 <BODY>
  <TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" class="bannercell">
     <A HREF="http://www.valleytennis.org/" CLASS="bannerlink">
      <IMG SRC="taller.png" ALT="" CLASS="vtbanner" WIDTH="630" HEIGHT="63">
     </A>
    </TD>
   </TR>
  </TABLE>

<!-- begin side bar -->
  <TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" >
     <TABLE BORDER="0" class="outersidebar"><TR><TD class="bordercell" VALIGN="TOP">
      <TABLE BORDER="0" CELLSPACING="3" class="vtsidebar"><TR><TD class="titlecell" VALIGN="TOP">
       <TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0" class="innersidebar">

        <TR><TD class="linkcell"><A HREF="./"><B>Apple Valley<BR>Tennis</B></A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="about.php">About the tennis<br> programs</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="coed.php">Summer Co-ed<br> Traveling Tennis<br> Team</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="tourn.php">Summer Tennis<br> Tournaments</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="camp_girls.php">Middle School<br> and High School <br>Summer Camps <br>for Girls</a> </TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt="">Apple Valley<br>High School<br> <a HREF="avhs_girls.php">Girls Team</a><br> <a href="avhs_boys.php">Boys Team</a></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><a HREF="tips.php">Previous Tips <br>of the day</a></TD></TR>

       </TABLE>
<!-- end side bar -->

       </TD>
      </TR>
     </TABLE>
     </TD>
    </TR>
   </TABLE>
   </TD>
   <TD VALIGN="TOP">




<TABLE BORDER="0"  CELLSPACING="2" CELLPADDING="0" WIDTH="100%">
 <TR>
  <TD ALIGN=CENTER VALIGN=TOP>


<!-- table for 1.0 Countdown -->
   <!-- table for black borders -->
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">

     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="titlecell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>