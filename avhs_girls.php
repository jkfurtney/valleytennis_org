<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <TITLE>Apple Valley Tennis</TITLE>
  <LINK REL="stylesheet" HREF="vt.css" TYPE="text/css">
  <LINK REL="shortcut icon" HREF="favicon.ico">
 </HEAD>
 <BODY>
  <TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" class="bannercell">
     <A HREF="http://www.valleytennis.org/" CLASS="bannerlink">
      <IMG SRC="taller.png" ALT="" CLASS="vtbanner" WIDTH="630" HEIGHT="63">
     </A>
    </TD>
   </TR>
  </TABLE>

<!-- begin side bar -->
  <TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" >
     <TABLE BORDER="0" class="outersidebar"><TR><TD class="bordercell" VALIGN="TOP">
      <TABLE BORDER="0" CELLSPACING="3" class="vtsidebar"><TR><TD class="titlecell" VALIGN="TOP">
       <TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0" class="innersidebar">

        <TR><TD class="linkcell"><A HREF="./"><B>Apple Valley<BR>Tennis</B></A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="about.php">About the tennis<br> programs</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="coed.php">Summer Co-ed<br> Traveling Tennis<br> Team</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="tourn.php">Summer Tennis<br> Tournaments</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="camp_girls.php">Middle School<br> and High School <br>Summer Camps <br>for Girls</a> </TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt="">Apple Valley<br>High School<br> <a HREF="avhs_girls.php">Girls Team</a><br> <a href="avhs_boys.php">Boys Team</a></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><a HREF="tips.php">Previous Tips <br>of the day</a></TD></TR>

       </TABLE>
<!-- end side bar -->

       </TD>
      </TR>

     </TABLE>
     </TD>
    </TR>
   </TABLE>
   </TD>
   <TD VALIGN="TOP">




<TABLE BORDER="0"  CELLSPACING="2" CELLPADDING="0" WIDTH="100%">
 <TR>
  <TD ALIGN=CENTER VALIGN=TOP>


<!-- table for 1.0 Countdown -->
   <!-- table for black borders -->
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">

     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="titlecell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>
Apple Valley High School Tennis girls team
          </B></TD><TD ALIGN=RIGHT NOWRAP><B>
<!-- Date of Countdown -->

          </B></TD></TR>
         </TABLE>
        </TD>
       </TR>
       <TR>
        <TD CLASS="contentcell">

         <!-- table for "content" cell with wide left/right margins -->
         <TABLE WIDTH="100%" BORDER="0" CELLPADDING="8" CELLSPACING="0">
          <TR>
           <TD WIDTH="5%"></TD>
           <TD WIDTH="90%">


<!-- cut here for template -->

<h3>Information from the coaches</h3>
<ul>
 <li><a href='#info3'>Message from the Captains</a>
 <li><a href='#info2'>AVHS girls team pre-season information</a>
 <li><a href='#info1'>AVHS girls team summer thoughts</a>
</ul>
<h3>Matches </h3>
<ul>
  <li><a href='avhsGirlsSched.php'>Schedule</a>
  <li><a href='avhsGirlsResults.php'>Results</a>
</ul>
<h3>Coach Emails</h3>
<ul>
<li><a href="mailto:coach@valleytennis.org">coach@valleytennis.org</a> Varsity and JV Coach
<li><a href="mailto:myounquist@aol.com">myounquist@aol.com</a> 9th grade coach
</ul>

<hr>
<a name='info3'></a><h3>Message from the Captains</h3>

<p>Hey Girls!
<p>We hope you are all having great summers and
especially getting out and playing lots of tennis.
There are many tournaments and other tennis playing
opportunities posted at www.valleytennis.org. We
encourage all of you to visit this web site. The 2002
girls' tennis season is just around the corner and
captain's practices begin on August 1 from 9am to
10am. 
<p>We'd also like to remind you all to make sure that
your medical records are updated at the office and
that you bring your green slip on the first day of
practice. You can obtain these slips at the office. On
August 15 we'll have our 2nd annual tennis feast at
Anne Schema's house. We'll be ordering uniforms here
so it is important that everyone tries to attend.
There will be more information about this on the first
day of practice. If you have any questions at all
please feel free to e-mail Yana Sorokin at
yanz03 at yahoo dot com
 
<p>We hope to see you all at Captain's Practices!

<p>Yana, Anne, and Steph



<!-- link for pre-season -->
<hr>
<a name='info2'></a><h3> Pre-Season information</h3>
The coaches are looking forward to the upcoming season that we hope will be fun, educational and rewarding.  We have a few pre-season thoughts.
<ul>
<li> We hope to run four teams this year.  Varsity, two JV teams and a ninth grade team.
<li> Please note that we would like all players to report to the Arena at  9am on Monday August 12th 2002.  We understand a letter from the school gives a different time, this was our fault, because we did not inform the athletic secretary of the correct time!
<li> We have had several questions about player's availability during the three weeks of practice before school starts.  We expect all girls who are interested in playing on the varsity and junior varsity team to be available every day.  We need to establish a preliminary varsity and junior varsity team by Friday August 16th, when we have our first match.  We hope that all other junior varsity and 9th grade players will attend practice each day.  
<li> All team positions will be decided by challenge matches that will continue well into the season.
<li> Players on the varsity and junior varsity team will play singles or doubles for the whole season.  Players on the second junior varsity team and the 9th grade team will play both singles and doubles during the season.
<li> All players must have filled in all the forms at the school office and give the green slip, provided by the athletic secretary, to one of the coaches before you will be allowed to start to practice.
<li> We will expect those players trying out for the varsity team to play up to two best of three set challenge matches in a day.  Matches will start on Tuesday August 13th.   
<li> Players are asked to choose their own doubles partners.  
<li> Players may try for singles first, and then if they do not make that line-up, they may try for doubles.
<li> Our philosophy is to play our strongest four players at singles and then pair the next 6 players as three doubles teams.
<li> There will be a parents meeting on Tuesday August 20th 2002 at 7pm in room 124.
<li> If anybody has any concerns or questions please e-mail the coaches, using the links on this page.
<li> Our hope is that our players are preparing for the season by working on their fitness and competing.  The best way to prepare for matches is to play matches.  There is a tournament run by the coaches at the Arena from August 2nd to the 4th.
<li> We will give more information about practice times and challenge match times by August 1st.
</ul>

<hr>
<!-- link for summer thought here -->


<a name='info1'></A><h3>Summer Thoughts</h3>

<p>Those of you who were on the team last year will have received a
letter from the coaches which looks a lot like this page!  We hope
that this also will be reached by our new ninth graders.  If you have
a friend who might be interested or you know a younger player please
share our web address or print this page for them if they do not have
internet access.


<p>We had a very successful season last year but we have lost three
seniors who were three-quarters of our singles line-up!  We hope to
run four teams again and will have four coaches in place.  A Varsity
team; two JV teams, brown and gold; and a 9th grade team.


<p>Our goal is to give you the opportunity to be as good at tennis as you
want to be.  You will reach the level of tennis you decide.  We will
help you in every way possible to realize your goal.

<p><b>Thoughts for summer improvement:</b>
<ol>
<li>	Watch tennis on television.        
<li> Read tennis books. 
<li> Attend a tennis stroke class.

<li> Attend a Tennis Camp.  There are many day and residential ones
in the area. It would be fun if a group could go together.  Our <a href="camp_girls.php">High
School Readiness Camp</a> for girls is at the Arena July 29th to Aug 1st 3
- 5pm $40 

<li> Play matches.  AVHS coaches Sue and Susans <a href="coed.php">Traveling Team</a> June 10th - July 29th - 6
weeks $60 - You can attend on a weekly basis

<li> Play tournaments.  The coaches organize <a href="tourn.php">2 tournaments</a> at the Arena.


<li> 	Work on your Physical Conditioning
 <ul>
  <li>Strength: The school has a summer weight program.
  <li>Agility: Sprints - either during longer runs or run the lines on the court.
  <li>Endurance: Try to build up to 3 mile runs 4 times a week.
 </ul>
</ol>


<p>Practice starts for the girls team on Monday August 12th 2000 - 9am at
the Arena.  Remember to fill in all forms and pay the fee <i>before</i> the
first day.  Your position on the team will be determined by challenge
matches.  You have to decide how hard you want to prepare for the
season.  On the first day of practice you will be asked if you wish to
try out for varsity singles.  After the 4 have been chosen by the end
of that week we will select the varsity doubles teams.  If you are
going to try for Varsity and/or JV doubles try and decide on your
partners now so that you can practice together.  The AVHS tennis
program welcomes all tennis players.  Everyone will play matches so
please encourage all your friends and neighbors to sign up for the
school program.  If you have any questions please e-mail us at
<a href="mailto:coach@valleytennis.org">coach@valleytennis.org</a>.


<!-- cut here for template -->

           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>

     </TD>
    </TR>
   </TABLE>

  </TD>
 </TR>
</TABLE>



</TD>
</TR>
<TR>
 <TD COLSPAN="2" ALIGN="RIGHT" VALIGN="TOP">
  <div class="documentinfo">
   Web hosting by <a href="http://www.luno.net">Luno.net</a>
   <br>
    <?php 
     putenv("TZ=America/Chicago");
     echo "Last modified: " . date ("F d Y h:ia. T", getlastmod());
    ?>
   <br>email web@valleytennis.org about technical problems
  </div>
 </TD>
</TR>
</TABLE>
</BODY>
</HTML>
