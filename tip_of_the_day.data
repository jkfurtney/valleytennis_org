Tip of the day file for valleytennis.org
#date format must be at the
#start of each line and one or more spaces must seperate the date from
#the begining of the tip. The tip should only occupy one line. Any line
#break in the file file will end the match as the file is simply
#matched against
#/^(\d{2}-\d{2}-\d{2})\s+(.*)$

# any line which is not a tip should be commented with a #

#start june
04-06-03 Placement is powerful.
04-07-03 Keep your head up, on the serve and overhead, until <i>after</i> you have hit the ball.
04-08-03 You are only as strong as your second serve.
04-09-03 Life is like a cup of tea, it is how you make it.
04-10-03 Watch the ball until you have hit the ball.
04-11-03 Never change a winning game.
04-12-03 "Tennis matches are not won with great shots.  They are won with many, many, pretty good shots."  Allen Fox
04-13-03 "Winners see what they want to happen while losers fear what might happen."  Skip Singleton
04-14-03 "Strive for progress, not perfection."  Anon
04-15-03 Keep your head up until well after you have hit the ball on your serve and overhead.
06-16-03 "Prepare for each point with a game plan.  Before you step up to the line to serve or receive you should know what you'd like to do."  Jack Groppel
06-17-03 "...three things that will make the game better for anybody: 1. Watch the ball - nothing else 2. Bend your knees as you hit 3. Get your first serve in. " Rod Laver
04-18-03 You cannot control the wind, but you can set the sails.
04-19-03 Be like a tea bag, show your strength when you are put in hot water.
04-20-03 Recover, Relax and Plan between points.
04-21-03 Keep your head still as you hit the ball.
04-22-03 Aim your winners to the gaps.
04-23-03 Lobs are passing shots.
04-24-03 Hit only shots you own.
04-25-03 Your best serve is the one your opponent likes the least.
04-26-03 Consistency is the ultimate weapon.
04-27-03 Develop rituals to repeat before you serve and return.
04-28-03 Learn to love the chaos of competition.
04-29-03 Life is not fair.
04-30-03 Breathe to relieve tension on the court.
#start july
05-01-03 The past and the future are your enemies in a tennis match.
05-02-03 Your doubles partner is your greatest asset.
05-03-03 Don't be amazing, just be effective.
05-04-03 No need to hit a ten dollar shot, when a dime shot will win the point.
05-05-03 "The most important thing to remember when serving in doubles is to get your first serve into play, even if it means toning it down and passing up attempts at aces."  Harry Hopman
05-06-03 "A smile wins a lot of points because it gives the impression of confidence on your part that shakes that of  the other man."  Bill Tilden
05-07-03 Turn your knuckles to the target on the backhand volley.
05-08-03 "The worse the conditions are, bright sun, high winds, the better for you - if you have the right outlook - because they are driving the other player nuts."  Arthur Ashe
05-09-03 "No where is it clearer than in competitive sport that you have to love it.  Love the grinding, the searching,  the pushing, the pulling, the victories, the lessons, the battle itself.  Becoming the best competitor you can  be means loving to compete more than winning.  Becoming the best you can be at anything means loving  the journey - from beginning to end."  Jim Loehr
05-10-03 Set several, simple, specific, attainable goals before you step on a court.
05-11-03 "It is not really my opinion, but also is that of Lacoste, that the points gained in tennis are less often the result of an opponent's victorious attack than the result of an error.  Eight times out of ten the cause of such error is the lack of attention paid to the ball."  Henri Cochet
05-12-03 "Nothing can disrupt a match, change its flow, or alter opponents' strategies more than a lob can."  Skip  Singleton
05-13-03 "Success is never final.  Failure never fatal.  It is courage that counts"  Winston Churchill
05-14-03 "Concentration is essential in tennis, far more than most people think.  It is a great help to the player who does not let 'unjust' line decisions upset him.  More than once this ability has made the difference between  losing and winning."  Budge Patty
05-15-03 "Use the middle on any ball that you cannot end the point on."  Chuck Kriese (On doubles)
05-16-03 "Depth is more important than power."  Allen Fox
05-17-03 "Resist the temptation to hit hard and win quick."  Brad Gilbert
05-18-03 "Tennis is played primarily with the mind."  Bill Tilden
05-19-03 "Never let your weight move back on any tennis stroke.  If your weight is not transferred forward, the ball  is moving you rather than you moving the ball."  Pancho Gonzales
05-20-03 Keep your eyes on the court, ball or strings between points.
05-21-03 Keep your mind on the moment.  Every point deserves your undivided attention.
05-22-03 "Be in sound physical shape.  You give your opponent a great psychological lift when you are breathing  heavily after the first set."  Vic Braden
05-23-03 "Tennis is a game played one point at a time no matter what the score is or who your opponent is."  Dr.  Jack Groppel
05-24-03 If pulled wide on the baseline - a lob will give you time to recover. When at the net, angle high balls down into the open court.
05-25-03 "Never lose sight of the fact that the overall objective is to communicate with your partner in such a way as  to make him/her play their best."  Allan Fox
05-26-03 "A tennis player should be able to lose and win without revealing in their attitude the value to which they  may attach the result.  This is the most essential law of sport:  one should never play tennis in order to win, one should try to play as well as possible and be content to have done one's best."  Rene Lacoste
05-27-03 "Let go of self doubt and embrace self-assurance."  The Feel Good Book
05-28-03 Players have control over their thoughts, their reactions, their strategy, their fitness, and their preparation.
05-29-03 Watching the ball longer on each hit will dramatically increase a player's consistency.
05-30-03 It is easier to hit the ball back the way it came.
05-31-03 Use different shaped swings to handle balls of different heights when you are hitting groundstrokes.
# start aug
06-01-03 Keep your eyes level with the ball on high volleys.
06-02-03 Keep your hand level with the ball on low volleys and half volleys.
06-03-03 During the point be sure to Watch and Aim the ball and return to an appropriate Ready position.
06-04-03 Take your time between points, start the next point only after you have recovered from the last point,  relaxed and planned that next point.
06-05-03 Recover in a well-balanced ready position by the time your ball bounces or is about to be volleyed.
06-06-03 Move two steps forward or two steps back immediately after you serve.
06-07-03 Keep your self-talk positive.
06-08-03 Make your opponent look good in the warm-up.  I repeat - in the warm-up!
06-09-03 Always start with a game plan.
06-10-03 On the high volley, decide early on your target, so you have time to turn your strings in that direction.
06-11-03 Go up and meet the ball as high as you can on the serve and overhead.
06-12-03 "Keep your eye on the ball and keep your mind on the game."  Bill Tilden
06-13-03 "Play happy and you'll love the game, you'll love your enthusiasm, and you'll improve because of it.  Just your own sheer enjoyment of the game can catapult you over the player who fights themselves on every point."  Peter Burwash
06-14-03 "Take at least one deep breath from your lower stomach before starting a point whenever you are feeling angry, frustrated, or feeling the pressure of play.  Breathe in through your nose and out through your mouth."  Jim Loehr
06-15-03 Exhale on contact when serving.
06-16-03 To improve your match performance concentrate on what you want to happen.
06-17-03 Even if you do not feel confident, ACT CONFIDENT!
06-18-03 Try to be at least three feet BEHIND the baseline after you serve if you have elected to stay back.
06-19-03 If you are hitting a ball on a groundstroke around shoulder height your follow-through should still finish at least as high as you made contact with the ball.  Gravity will bring it down!
06-20-03 Prepare on court cures for problems you may encounter during a match.
06-21-03 The player with the fewest errors wins.
06-22-03 Strive for consistent returns of serve.  Treat them with the same care as you d your second serve.
06-23-03 "Statistics prove that many more points are won when first serves are hit in.  It also helps to keep the pressure on your opponents."  Skip Singleton 
06-24-03 Find the amount of muscle tension that produces your best results, and plan way to attain and maintain that in a match.
06-25-03 Appear imperturbable during your matches.
06-26-03 "Learn to play your own best game."  Harry Hopman
06-27-03 A variety of well-placed serves will cause havoc for your opponent.
06-28-03 Half volleys should be hit at half pace.
06-29-03 Early in a match aim further inside the lines and higher over the net.
06-30-03 Use the highest percentage shot available to win the point.
06-31-03 Controlled bodies hit controlled shots.
# start sept
07-01-03 Establish a match preparation routine you follow religiously each time you play.
07-02-03 Have a simple, specific, attainable game plan written down before each match.
07-03-03 During the warm-up get yourself physically relaxed, mentally alert and emotionally sound!
07-04-03 When aiming a ball, have a very clear picture in your head of the direction, depth, speed and spin you want.
07-05-03 Matches are won, not with great shots, but with many, many, non-flashy, common and garden, bread and butter, boring shots.
07-06-03 Number one, have fun.
07-07-03 Rotate your upper body until your belly button faces the target on serves, overheads, forehands and two handed backhands.
07-08-03 Try to use your whole body to hit the ball - it's a joint effort!
07-09-03 Prepare your racket promptly.  
07-10-03 Some things in your match will not be to your liking.  Most of these are beyond your control but you can control the way you react.
07-11-03 Shift your weight towards your target just before hitting the ball so your momentum is transferred into the shot.
07-12-03 "Foot-faulting is cheating."  The Code
07-13-03 The next time you encounter the impossible craziness, clench your fists, get a determined smile on your face and, with all the feeling and emotion you can muster, say to yourself - I LOVE IT!"  Jim Loehr
07-14-03 Keep your racket in your non-dominant hand between points.
07-15-03 "Do not strive for terrific speed at first.  The most essential thing about a drive is to put the ball in play.  Accuracy first, and then put on your speed."  Bill Tilden - The Art of Tennis
07-16-03 When at the net in doubles imagine every ball is coming to you, plan which gap to aim for, so you are prepared when you are able to hit the ball.
07-17-03 Shadow the correct form of the shot you just missed, to help you let your error go.
07-18-03 Finish HITTING the ball before LOOKING to see if you hit it in!
07-19-03 I am relaxed and focused.  A positive affirmation to use to help these desired conditions prevail in your match.  Compose your own and repeat them to yourself to help you achieve your goals.
07-20-03 How do you react when fear rears its ugly head in a match?  Develop the courage to recognize you are scared and find routines that will help you focus on what you need to do at this moment to maximize your chances of winning the next point.
07-21-03 Do your very best on every point.  That is success in a tennis match.
07-22-03 "... there is no better way to slow down a hitter than by reducing the pace of your shots with slow, deep slices and chops, forcing him to manufacture his own power." Budge Patty 1951
07-23-03 Good offense will beat good defense but very good defense will beat good offense.  
07-24-03 
07-25-03 
07-26-03 
07-27-03 
07-28-03 
07-29-03 
07-30-03 
# start oct
