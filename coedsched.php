<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
 <HEAD>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <TITLE>Apple Valley Tennis</TITLE>
  <LINK REL="stylesheet" HREF="vt.css" TYPE="text/css">
  <LINK REL="shortcut icon" HREF="favicon.ico">
 </HEAD>
 <BODY>
  <TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" class="bannercell">
     <A HREF="http://www.valleytennis.org/" CLASS="bannerlink">
      <IMG SRC="taller.png" ALT="" CLASS="vtbanner" WIDTH="630" HEIGHT="63">
     </A>
    </TD>
   </TR>
  </TABLE>

<!-- begin side bar -->
  <TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
   <TR>
    <TD VALIGN="TOP" >
     <TABLE BORDER="0" class="outersidebar"><TR><TD class="bordercell" VALIGN="TOP">
      <TABLE BORDER="0" CELLSPACING="3" class="vtsidebar"><TR><TD class="titlecell" VALIGN="TOP">
       <TABLE CELLPADDING="2" CELLSPACING="2" BORDER="0" class="innersidebar">

        <TR><TD class="linkcell"><A HREF="./"><B>Apple Valley<BR>Tennis</B></A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="about.php">About the tennis<br> programs</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="coed.php">Summer Co-ed<br> Traveling Tennis<br> Team</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="tourn.php">Summer Tennis<br> Tournaments</A></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><A HREF="camp_girls.php">Middle School<br> and High School <br>Summer Camps <br>for Girls</a> </TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt="">Apple Valley<br>High School<br> <a HREF="avhs_girls.php">Girls Team</a><br> <a href="avhs_boys.php">Boys Team</a></TD></TR>

	<TR><TD class="linkcell"><img src="smallball.png" alt=""><a HREF="tips.php">Previous Tips <br>of the day</a></TD></TR>

       </TABLE>
<!-- end side bar -->

       </TD>
      </TR>
     </TABLE>
     </TD>
    </TR>
   </TABLE>
   </TD>
   <TD VALIGN="TOP">




<TABLE BORDER="0"  CELLSPACING="2" CELLPADDING="0" WIDTH="100%">
 <TR>
  <TD ALIGN=CENTER VALIGN=TOP>


<!-- table for 1.0 Countdown -->
   <!-- table for black borders -->
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">

     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="titlecell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>
Schedule for Co-ed Traveling Team
          </B></TD><TD ALIGN=RIGHT NOWRAP><B>
<!-- Date of Countdown -->

          </B></TD></TR>
         </TABLE>
        </TD>
       </TR>
       <TR>
        <TD CLASS="contentcell">

         <!-- table for "content" cell with wide left/right margins -->
         <TABLE WIDTH="100%" BORDER="0" CELLPADDING="8" CELLSPACING="0">
          <TR>
           <TD WIDTH="5%"></TD>
           <TD WIDTH="90%">


<!-- cut here for template -->
<h3>Co-ed Traveling Team Matches</h3>

<p>These are the matches the coaches have arranged for the teams at this time. 
We have more irons in the fire and expect to add many more matches.  

<p>Please let the coaches know which matches you wish to play.  Please
e-mail <a href="mailto:coach@valleytennis.org">coach@valleytennis.org</a> or call the telephone numbers handed
out by the coaches.  

<p>Each Monday we will have in-house matches at the
Arena 1-4pm.  We will add more times if needed.

<p>Check back often as the most up to date information will be here.


<p>
<?php
// test code for matching _schedual_.data
// needs to make it self in to a human readable calender
putenv("TZ=America/Chicago");   
#error_reporting(0);             

$msbgc = "#FFFF66";
$jvbgc = "#99CCFF";
$vbgc  = "#CCCCCC";
$cbgc  = "#FFCCFF";
$todaybgc = "#00ff99";

$weekdaynames[0] = 'Sun.';
$weekdaynames[1] = 'Mon.';
$weekdaynames[2] = 'Tue.';
$weekdaynames[3] = 'Wed.';
$weekdaynames[4] = 'Thu.';
$weekdaynames[5] = 'Fri.';
$weekdaynames[6] = 'Sat.';


$file = file('_schedual_.data');
$date =  date(m) ."-". date(d)."-" .date(y);
$fancy_date = date(l)." the ".date(jS)." of ".date(F)."<br>";
$start = 0;


foreach ($file as $line) {
#    echo $line ."<br>";
  if (preg_match("/^(\d{2}-\d{2}-\d{2})\s+MS\s*\[\s*\'(.*)\'\s*,\s*\'(.*)\'\s*\]\s+JV\s*\[\s*\'(.*)\'\s*,\s*\'(.*)\'\s*\]\s+V\s*\[\s*\'(.*)\'\s*,\s*\'(.*)\'\s*\]\s+C\s*\[\s*\'(.*)\'\s*\].*/", $line, $match)) {
    if (($match[1] == $date) ) { $start++; }
    if ($start) {$results[] = $match;}
  }
}
 
echo "<table border=1>
<tr><th>Colour</th><th>Group</th></tr>
<tr bgcolor=\"$msbgc\"><td>&nbsp</td><td>Middle School</td></tr>
<tr bgcolor=\"$jvbgc\"><td>&nbsp</td><td>Junior Varsity</td></tr>
<tr bgcolor=\"$vbgc\"><td>&nbsp</td><td>Varsity</td></tr>
<tr bgcolor=\"$cbgc\"><td>&nbsp</td><td>Information for Everyone</td></tr>
</table>";

 
echo "<p><table border=1 width=\"100%\">";
echo "<tr><th>date</th><th>info</th></tr>";
foreach ($results as $day) {

  if (preg_match("/^(\d{2})-(\d{2})-(\d{2})/", $day[1], $datearray )) {
    $datehash = getdate(mktime(12,0,0,$datearray[1], $datearray[2], $datearray[3]));
    $dayofweek = $weekdaynames[$datehash['wday']];
#    echo $dayofweek . "<br>";
      }

  
  if ($day[1] == $date) {echo "<tr><td bgcolor=\"$todaybgc\">".$dayofweek." ".$day[1]."</td><td>";}
  
  else{echo "<tr><td>".$dayofweek." ".$day[1]."</td><td>";}

  if ($day[2] or $day[3] or $day[4] or $day[5] or $day[6] or $day[7] or $day[8]) {
    echo "<table border=0 width=\"100%\">";
    if ($day[2] or $day[3]) {
      echo "<tr bgcolor=\"$msbgc\"><td>[ MS ] $day[2] $day[3]</td></tr>";
    }
    if ($day[4] or $day[5]) {
      echo "<tr bgcolor=\"$jvbgc\"><td>[ JV ]  $day[4] $day[5]</td></tr>";
    }
    if ($day[6] or $day[7]) {
      echo "<tr bgcolor=\"$vbgc\"><td>[ V ] $day[6] $day[7]</td></tr>";
    }
    if ($day[8]) {
      echo "<tr bgcolor=\"$cbgc\"><td>$day[8]</td></tr>";
    }


    echo "</table>";
  }
  

  echo "</td></tr>";
}
echo "</table>";


    //    echo $match[1]." - 2".$match[2]." - 3".$match[3]."<br>";
    
    // read match data into a multidimentional array
    // list of lists!



#    echo "<br>";
    // 1 is the entire thing
    // 2 is the date is mm-dd-yy
    // (3,4)  is ms (place , time)
    // (5,6)  is jv (place , time)
    // (7,8)  is v (place , time)
    // 9 is the comment
    // still need to check patern over carefilly but looks ok
#    foreach ($match as $m) {
#      $c++;
#      if ($m) echo $c." ".$m." <br>";      
#    }

#    echo "<br>";

 



?>



<!-- cut here for template -->


           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>

     </TD>
    </TR>
   </TABLE>

  </TD>
 </TR>
</TABLE>



</TD>
</TR>
<TR>
 <TD COLSPAN="2" ALIGN="RIGHT" VALIGN="TOP">
  <div class="documentinfo">
   Web hosting by <a href="http://www.luno.net">Luno.net</a>
   <br>
    <?php 
     putenv("TZ=America/Chicago");
     echo "Last modified: " . date ("F d Y h:ia. T", getlastmod());
    ?>
   <br>email web@valleytennis.org about technical problems
  </div>
 </TD>
</TR>
</TABLE>
</BODY>
</HTML>
