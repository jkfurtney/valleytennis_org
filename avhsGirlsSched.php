<?php  include("header1.php"); ?>

Schedule for AVHS Boys Team

<?php  include("header2.php"); ?>


<h3>AVHS Boys Team Matches</h3>


<a href="mailto:coach@valleytennis.org">coach@valleytennis.org</a>

<p>Check back often as the most up to date information will be here.

<p><a href="printerSched.php">Printer Friendly Version</a>

<p>
<?php
// test code for matching _schedual_.data
// needs to make it self in to a human readable calender
putenv("TZ=America/Chicago");   
error_reporting(0);             

$varsc = "#77ffe8";   # color table: varsity   #FFFF66
$jvbc  = "#ffcb8c";   # jv brown
$jvgc  = "#fff78c";   # jv gold
$ninec = "#FFCCFF";   # 9th
$anoc  = "#99CCFF";   # anoncment 


$todaybgc = "#00ff99";

$weekdaynames[0] = 'Sun.';
$weekdaynames[1] = 'Mon.';
$weekdaynames[2] = 'Tue.';
$weekdaynames[3] = 'Wed.';
$weekdaynames[4] = 'Thu.';
$weekdaynames[5] = 'Fri.';
$weekdaynames[6] = 'Sat.';


$file = file('_avhs_sched.data');
$date =  date(m) ."-". date(d)."-" .date(y);
$fancy_date = date(l)." the ".date(jS)." of ".date(F)."<br>";
$start = 0;


foreach ($file as $line) {
# $match is the array of matched things
  if (preg_match("/^(\d{2}-\d{2}-\d{2})\s+V\s+>>(.*)<<\s+JVB\s+>>(.*)<<\s+JVG\s+>>(.*)<<\s+9\s+>>(.*)<<\s+E\s+>>(.*)<</", $line, $match)) {
# with this match 
# $match[1] = the date in mm-dd-yy format
# $match[2] = Vars
#        3  = jvb
#        4  = jvg
#        5  = 9
#        6  = info for all
    if (($match[1] == $date) ) { $start++; } 
    if ($start) {$results[] = $match;}
  }
}
 
echo "<table bgcolor=\"#009933\" cellspacing=3>
<tr bgcolor=white><th>Colour</th><th>Group</th></tr>
<tr bgcolor=\"$varsc\"><td>&nbsp;</td><td>Varsity</td></tr>
<tr bgcolor=\"$jvbc\"><td>&nbsp;</td><td>JV Brown</td></tr>
<tr bgcolor=\"$jvgc\"><td>&nbsp;</td><td>JV Gold</td></tr>
<tr bgcolor=\"$ninec\"><td>&nbsp;</td><td>9th Grage</td></tr>
<tr bgcolor=\"$anoc\"><td>&nbsp;</td><td>Information for everyone</td></tr>
</table>";

 
echo "<p><table bgcolor=\"#009933\" cellspacing=3 width=\"100%\">";
echo "<tr bgcolor=white><th>date</th><th>info</th></tr>";
foreach ($results as $day) {

  if (preg_match("/^(\d{2})-(\d{2})-(\d{2})/", $day[1], $datearray )) {
    $datehash = getdate(mktime(12,0,0,$datearray[1], $datearray[2], $datearray[3]));
    $dayofweek = $weekdaynames[$datehash['wday']];
      }

  if ($day[1] == $date) {echo "<tr><td bgcolor=\"$todaybgc\">".$dayofweek." ".$day[1]."</td><td bgcolor=white>";}
  
  else{echo "<tr bgcolor=white><td>".$dayofweek." ".$day[1]."</td><td>";}

  if ($day[2] or $day[3] or $day[4] or $day[5] or $day[6]) {
    echo "<table border=0 width=\"100%\">";
    if ($day[2]) {
      echo "<tr bgcolor=\"$varsc\"><td>[ Vars ] $day[2] </td></tr>";
    }
    if ($day[3]) {
      echo "<tr bgcolor=\"$jvbc\"><td>[ JVb ] $day[3] </td></tr>";
    }
    if ($day[4]) {
      echo "<tr bgcolor=\"$jvgc\"><td>[ JVg ] $day[4] </td></tr>";
    }
    if ($day[5]) {
      echo "<tr bgcolor=\"$ninec\"><td>[ 9th ] $day[5] </td></tr>";
    }
    if ($day[6]) {
      echo "<tr bgcolor=\"$anoc\"><td>$day[6]</td></tr>";
    }

    echo "</table>";
  }
  

  echo "</td></tr>";
}
echo "</table>";


?>
<!-- cut here for template -->
           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>

     </TD>
    </TR>
   </TABLE>

  </TD>
 </TR>
</TABLE>

</TD>
</TR>
<TR>
 <TD COLSPAN="2" ALIGN="RIGHT" VALIGN="TOP">
  <div class="documentinfo">
   Web hosting by <a href="http://www.luno.net">Luno.net</a>
   <br>
    <?php 
     putenv("TZ=America/Chicago");
     echo "Last modified: " . date ("F d Y h:ia. T", getlastmod());
    ?>
   <br>email web@valleytennis.org about technical problems
  </div>
 </TD>
</TR>
</TABLE>
</BODY>
</HTML>
