<?php include("header1.php");   ?>
Co-ed Traveling Junior Tennis Team
<?php include("header2.php");   ?>

<!-- cut here for template -->

<h3>Co-ed Traveling Junior Tennis Team</h3>
June 9th through July 24th 2003

<p> Open to 9 - 12th grades

<p>Cost $60

<p>The AVHS varsity tennis coaches will arrange matches against different
teams within the metro area.  The matches will give players the
opportunity to compete in a friendly environment under the coaches
supervision.
  
<p>At least one match per week will be offered and will be organized
through this website, www.valleytennis.org or telephone.  A T-shirt
will be provided.  In-house matches will be held at AVHS on Mondays
from 2 - 4pm each week Please come to all matches prepared for all
eventualities.  WATER IS A MUST!  Any questions feel free to e-mail
us.
 


 <a href="mailto:coach@valleytennis.org">coach@valleytennis.org</a>

<!-- <p><a href="coedsched.php">View the match schedule.</a> -->

<!-- cut here for template -->

<?php include('tail.php'); ?>
