
Co-ed USTA Blue Team (Traveling Junior Tennis Team)	              June 9th through July 24th 2003
Open to 9 - 12th grades

Cost $60

Sectional Tournament July 17th and 18th (all day)


The AVHS varsity tennis coaches will arrange matches against different teams within the 
metro area.  The matches will give players the opportunity to compete in a friendly 
environment under the coaches supervision.
  
At least one match per week will be offered and will be organized through this website,  
www.valleytennis.org or telephone.  
A T-shirt will be provided.
In-house matches will be held at AVHS on Mondays from 2 - 4pm each week Please come to all matches prepared for all eventualities.  WATER IS A MUST!
Any questions feel free to e-mail us.
 
