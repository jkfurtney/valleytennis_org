<?php include("header1.php");   ?>
Middle School and High School Summer Camps 
<?php include("header2.php");   ?>


<!-- cut here for template -->
<h3>Middle School Co-ed Camp 6th-8th</h3>

<p>Work with the AVHS varsity tennis coaches.  All aspects of team tennis
will be covered: strokes, strategy, mental toughness, rules,
etiquette.  24 campers max.

<p>
<table border=0>
<tr><td>Dates:&nbsp&nbsp&nbsp&nbsp</td><td>August 4th - August th 2003
<br>Monday through Thursday
</td></tr> <tr><td>
Time:</td><td>9:00 - 10:30am
</td></tr>
<tr><td>Cost:</td><td>
$36
</td></tr>
</table>

<h3>High School Readiness Camp</h3>

<p>9th, 10th, 11th and 12th Grade Girls
For all girls interested in playing on the 2003 high school tennis team.  We will cover all aspects of high school team tennis  
24 campers max.

<p>
<table border=0>
<tr><td>Dates:&nbsp&nbsp&nbsp&nbsp</td><td>
July 7th - July 10th 2003
<br>Monday through Thursday
</td></tr> <tr><td>Time:</td><td>
Varsity 9:00 - 11:00 am <br> JV 11:30 - 1:00 pm
</td></tr>
<tr><td>Cost:</td><td>Varsity: $48 <br> JV: $36
</td></tr>
</table>

<p>Sign up for these camps at the Apple Valley Tennis Arena or call (952) 953-2366

<!-- cut here for template -->
<?php include('tail.php'); ?>
