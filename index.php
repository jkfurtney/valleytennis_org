<?php
// main title page!!
include("header1.php");   ?>
Welcome to ValleyTennis.org!

<?php include("header2.php");   ?>

	   <!-- Body of topleft --> 
          <p>This web page is designed to help the Apple 
          Valley high school tennis coaches share information about their
          programs with interested players.
	  <p><a href="about.php">Learn more about the programs offered</a>
          <!-- End of Body of top left -->


           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>

     </TD>
    </TR>
   </TABLE>
   <!-- end of table for Mozilla Countdown -->

<!-- Table used to separate the tree status and download mozilla tables -->
<TABLE BORDER="0"><TR><TD></TD></TR></TABLE>

<!-- table for Mozilla News -->
   <!-- table for black borders -->
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">

     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="titlecell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>
	 Tip of the Day
          </B></TD><TD ALIGN=RIGHT NOWRAP><B>


<!-- php code for current date    -->
           <?php 
            putenv("TZ=America/Chicago");   
            error_reporting(0);             
            $fancy_date = date(l)." the ".date(jS)." of ".date(F)."<br>";
            echo $fancy_date;
            ?>
<!-- php code for current date    -->


          </B></TD></TR>
         </TABLE>
        </TD>
       </TR>
       <TR>
        <TD CLASS="contentcell">

         <!-- table for "content" cell with wide left/right margins -->
         <TABLE WIDTH="100%" BORDER="0" CELLPADDING="8" CELLSPACING="0">
          <TR>
           <TD WIDTH="5%"></TD>
           <TD WIDTH="90%">


	    <!-- php code for tip of the day  -->
            <?php 
	    putenv("TZ=America/Chicago");   
	    error_reporting(0);             


	    $file = file('tip_of_the_day.data');
	    $date =  date(m) ."-". date(d)."-" .date(y);
	    // $fancy_date = date(l)." the ".date(jS)." of ".date(F)."<br>";

	    foreach ($file as $tip) {
	      if (preg_match("/^(\d{2}-\d{2}-\d{2})\s+(.*)$/", $tip, $match)) {
	          if ($match[1] == $date) {
		        $tip_of_the_day =  $match[2]."<br>";
			break;
		  }
	      }
	    }


	    if ($tip_of_the_day) {
	      echo $tip_of_the_day."<br>";
	    }
	    else {
	      echo "Keep your head up, on the serve and overhead, until <i>after</i> you have hit the ball.<br>";
	    }
            ?>

            <!-- end tip of the day -->


           <DIV ALIGN="RIGHT"><A HREF="tips.php"><B>more...</B></A></DIV>

           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>

     </TD>
    </TR>
   </TABLE>
<!-- Table used to separate the status update and download mozilla tables -->
</TD><TD><TABLE BORDER="0" CELLSPACING="1" CELLPADDING="0"><TR><TD></TD></TR></TABLE>
</TD><TD VALIGN=TOP>
<!-- table for Status Update-->
   <!-- table for black borders -->
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">
     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="titlecell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>
           <!-- Title TOP RIGHT -->
AVHS boys tennis information
           <!-- end Title TOP RIGHT -->

          </B></TD><TD ALIGN=RIGHT NOWRAP><B>
<!-- Date of Status Update-->

          </B></TD></TR>
         </TABLE>
        </TD>
       </TR>
       <TR>

        <TD CLASS="contentcell">

         <!-- table for "content" cell with wide left/right margins -->
         <TABLE WIDTH="100%" BORDER="0" CELLPADDING="8" CELLSPACING="0">
          <TR>
           <TD WIDTH="5%"></TD>
           <TD WIDTH="90%">
	   <!-- TOP RIGHT-->


	   Check the <a href="avhsGirlsSched.php">schedule</a> for team match, dates, times, and locations. Also <a href="avhsGirlsResults.php">results</a> are now available.


	   <!-- END TOP RIGHT-->

           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>



     </TD>
    </TR>
   </TABLE>

<TABLE BORDER="0"><TR><TD></TD></TR></TABLE>
<TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">
     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="acell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>
	   <!-- Title middle RIGHT -->
	   Announcements	   
	   <!-- END Title midle RIGHT -->
<?php
# get all lines from data file ano.data
# and find out when it was last updated
# if the file does not exist put up a blank message
	    putenv("TZ=America/Chicago");   
	    error_reporting(0);             

            $filename = 'ano.data';
            if (is_file($filename)){
	      $file2 = file($filename);
	      $updated = date('F jS', filemtime($filename));
	    }
            else {
	      $file2[] = 'There are no announcements at the moment.';
	      $updated = '';
	    }
?>

          </B></TD><TD ALIGN="RIGHT" NOWRAP><B>
<?php
	      echo $updated;
?>

	  </B></TD></TR>
         </TABLE>
        </TD>
       </TR>
       <TR>
        <TD CLASS="contentcell">
         <!-- table for "content" cell with wide left/right margins -->
         <TABLE WIDTH="100%" BORDER="0" CELLPADDING="8" CELLSPACING="0">
          <TR>
           <TD WIDTH="5%"></TD>
           <TD WIDTH="90%">
	      

           <!-- middle right text -->
<?php
	 foreach ($file2 as $line){
	   echo $line;
	 }
?>


           <!-- END Middle  right text -->

<!-- End of Body of 1.0 Countdown -->

<!-- cut here for template -->

           </TD>
           <TD WIDTH="5%"></TD>
          </TR>
         </TABLE>

        </TD>
       </TR>
      </TABLE>

     </TD>
    </TR>
   </TABLE>





<!-- Table used to separate the tree status and download mozilla tables -->
<TABLE BORDER="0"><TR><TD></TD></TR></TABLE>
<!-- table for Download Mozilla -->
   <!-- table for black borders -->
   <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%">
    <TR>
     <TD CLASS="bordercell">
     <!-- table for white cells -->
      <TABLE BORDER="0" CELLSPACING="4" CELLPADDING="4" WIDTH="100%">
       <TR>
        <TD CLASS="titlecell">
         <TABLE BORDER="0" CELLSPACING="0" CELLPADDING="0" WIDTH="100%"><TR><TD><B>
	   <!-- Title BOTTUM RIGHT -->
           Contact Info
	   <!-- END Title BOTTUM RIGHT -->
          </B></TD><TD ALIGN="RIGHT" NOWRAP>
          </TD></TR>
         </TABLE>
        </TD>
       </TR>
       <TR>
        <TD CLASS="contentcell">
         <!-- table for "content" cell with wide left/right margins -->
         <TABLE WIDTH="100%" BORDER="0" CELLPADDING="8" CELLSPACING="0">
          <TR>
           <TD WIDTH="5%"></TD>
           <TD WIDTH="90%">
	      

           <!-- Bottum right text -->
	   Email: <a href="mailto:coach@valleytennis.org">coach@valleytennis.org</a> to contact the coaches.
           <!-- END Bottum right text -->

<!-- End of Body of 1.0 Countdown -->

<!-- cut here for template -->

<?php include('tail.php'); ?>